# -*- coding: utf-8 -*-
"""Selecting And Filtering Data.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1V4XL7iKg-YowhRlVxvE1jKSWvoR6R6rW
"""

!apt-get update
!apt-get install openjdk-8-jdk-headless -qq > /dev/null
!wget -q http://archive.apache.org/dist/spark/spark-2.3.1/spark-2.3.1-bin-hadoop2.7.tgz
!tar xf spark-2.3.1-bin-hadoop2.7.tgz
!pip install -q findspark

import os
os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-8-openjdk-amd64"
os.environ["SPARK_HOME"] = "/content/spark-2.3.1-bin-hadoop2.7"

!ls

import findspark
findspark.init()

import pyspark
from pyspark.sql import SparkSession
spark = SparkSession.builder.getOrCreate() 
spark

from pyspark.sql.types import *
myschema = StructType([
  StructField('id', IntegerType()),
  StructField('first_name', StringType()),
  StructField('last_name', StringType()),
  StructField('gender', StringType()),
  StructField('city', StringType()),
  StructField('job_title', StringType()),
  StructField('Salary', StringType()),
  StructField('latitude', FloatType()),
  StructField('longitude', FloatType())
])

df = spark.read.csv("original.csv", header=True, schema=myschema)
df.show()

df_select = df.select("first_name","last_name")
df_select.show()

df_renamed = df.withColumnRenamed('first_name', 'fn')
df_renamed.show()

df_filter = df.filter((df.first_name == 'Alvera'))
df_filter.show()

df_filter = df.filter((df.first_name.like("%lver%")))
df_filter.show()

df_filter = df.filter((df.first_name.endswith('din')))
df_filter.show()

df_filter = df.filter((df.first_name.startswith('Alv')))
df_filter.show()

df_filter = df.filter((df.id.between(1,5)))
df_filter.show()

df_filter = df.filter((df.first_name.isin('Aldin', 'Valma')))
df_filter.show()

df_substr = df.select(df.first_name, df.first_name.substr(1,5).alias('name'))
df_substr.show()

df_filter = df.filter((df.first_name.isin('Aldin', 'Valma')) | (df.city.like('%ondon')))
df_filter.show()

df_filter = df.filter((df.id > 10) & (df.id < 100))
df_filter.show()

